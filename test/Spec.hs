{-# LANGUAGE OverloadedStrings #-}
module Main where

import Lib

import Data.Text as T
import Control.Monad.Reader
import System.Console.ANSI
import System.Exit (exitFailure)

assert :: String -> Bool -> IO Bool
assert 
    testTitle
    test =
        do
            let printColorfulResult txt col = do
                    putStr "["
                    setSGR [SetColor Foreground Vivid col]
                    putStr txt
                    setSGR [Reset]
                    putStr "] "
                
            if test
                then printColorfulResult " OK " Green
                else printColorfulResult "FAIL" Red

            putStrLn testTitle
            return test


simpleConfig :: Config
simpleConfig = Config "KEY" "user@example.com" (Just 1)

simpleMkYaXMLURL t = runReader (mkYaXMLURL t) simpleConfig

testMkYaXMLURL :: Bool
testMkYaXMLURL =
    simpleMkYaXMLURL "Автомобили" == "http://xmlproxy.ru/search/xml?query=%D0%90%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D0%B8&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D5.docs-in-group%3D3&maxpassages=3&page=1&user=user%40example.com&key=KEY"

main :: IO ()
main = do
    putStrLn ""
    putStrLn "------------------------------"
    putStrLn "Running tests now..."
    
    result <-
        sequence [ 
            assert "Building propper url from config vars" testMkYaXMLURL
        ]

    putStrLn "------------------------------"
    if and $ result
       then putStrLn "Success!"
       else exitFailure
