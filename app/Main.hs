module Main where

import Lib
import Options.Applicative
import Data.Semigroup ((<>))

parseConf :: Parser Config
parseConf = Config
     <$> argument str 
         (metavar "XMLPROXY_API_KEY"
         <> help "Your key for http://xmlproxy.ru")
     <*> argument str 
         (metavar "XMLPROXY_LOGIN"
         <> help "Your login at http://xmlproxy.ru")
     <*> (optional . option auto $
             long "threads"
          <> metavar "THREADS_NUMBER"
          <> short 't'
          <> showDefault
          <> help "Number of threads (Default: number of CPUs)")

main :: IO ()
main = do
    let opts =
         info (parseConf <**> helper) $ 
         fullDesc <> progDesc "Yandex Clusterer helper (using xmlproxy.ru API)"
    putStrLn . show =<< execParser opts
