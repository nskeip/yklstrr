{-# LANGUAGE OverloadedStrings #-}
module Lib where

import qualified Data.Text as T
import Control.Monad.Reader
import Data.Semigroup
import Network.URI.Encode (encodeText)

type URL = T.Text
type Login = T.Text
type SearchRequest = T.Text

data SearchResult = SearchResult {
  title   :: T.Text,
  snippet :: T.Text,
  link    :: URL
} deriving Show

type SearchResultsPage = [SearchResult]

data Config = Config {
  apiKey  :: T.Text,
  apiUser :: Login,
  threads :: Maybe Int
} deriving Show

mkYaXMLURL :: SearchRequest -> Reader Config URL
mkYaXMLURL sr = do
  k <- asks apiKey
  u <- asks apiUser
  return $ 
    mconcat 
      [
        "http://xmlproxy.ru/search/xml?query=",
        encodeText sr,
        "&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D5.docs-in-group%3D3&maxpassages=3&page=1",
        "&user=",
        encodeText u,
        "&key=",
        k
      ]

extractFromXML :: T.Text -> Maybe [SearchResult]
extractFromXML = undefined

getTop10 :: SearchRequest -> IO SearchResultsPage
getTop10 = undefined

getPagesForAllRequests :: [SearchRequest] -> IO [SearchResultsPage]
getPagesForAllRequests = traverse getTop10
